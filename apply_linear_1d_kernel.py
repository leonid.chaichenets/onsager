#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sys import stdout
from argparse import ArgumentParser
import numpy as np
import pandas as pd

def __parse_cmd_line():
    """Parse command line arguments."""

    description = ('Parse a log file produced with the config  '
                       'https://github.com/CSMMLab/KiT-RT/blob/master/examples/configs/DataGeneratorClassification1D'
                       '.cfg containing discretized velocity distributions and put out the corresponding discretized '
                       'collision kernel.' )
    parser = ArgumentParser(description = description)
    parser.add_argument('filename',
        help = 'Filename of the log file.',
        metavar = 'FILE',
        nargs = '?',
        default = 'a5_ev5.csv'
    )
    parser.add_argument('-o', '--output',
        help = 'Filename of the output file.',
        metavar = 'OUT',
        nargs = '?',
        default = stdout
    )

    return parser.parse_args()

def __parse_log(filename):
    """Parse log in filename and return Gauss-Legendre quadrature parameters and data."""

    df = pd.read_csv(filename, index_col=0)

    # Cast column names (discrete velocities) to float.
    df.rename(lambda s: float(s), axis=1, inplace=True)

    # Get Gauss-Legendre quadrature parameters and drop the weights from imported data.
    qs = df.iloc[0]
    df.drop(index=df.index[0], inplace=True)
    #df.drop(index=df.index[2:], inplace=True)
    #df = df.iloc[:, [48, 50]]

    return qs, df

def q(qs, fs):
    """Apply the collision operator to the discretized velocity distribution fs using Gauss-Legendre quadrature parameters qs."""

    ran = qs.index.max() - qs.index.min()

    return -fs.sub((qs * fs).apply(np.sum, axis=1)/ran, axis=0)

if __name__ == '__main__':
    args = __parse_cmd_line()
    qs, fs = __parse_log(args.filename)

    Q = q(qs, fs)
    Q.to_csv(args.output)


#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import keras

@keras.saving.register_keras_serializable()
class kernelnet_continuous(keras.Model):
    def __init__(self,
                 vs,
                 ws,
                 dim = 1,
                 trunk_width = 40,
                 trunks = 3,
                 branch_width = 40,
                 f_nodes_number=100,
                 l = None,
                 tau = None,
                 beta = None,
                 **kwargs):
        super().__init__(**kwargs)
        # Separate out the nodes and the weights.
        self.vs = vs
        self.ws = ws

        self.tw = trunk_width
        self.bw = branch_width
        self.p = trunks
        self.d = dim
        self.m = f_nodes_number

        # Cholesky l_i factor as a tf.Model:
        if not l:
            self.l = lower_triangular(
                keras.models.Sequential([
                keras.layers.Input(shape=(self.d * 2,)),
                keras.layers.Dense(self.tw, activation='tanh'),
                keras.layers.Dense(self.tw, activation='tanh'),
                keras.layers.Dense(self.tw, activation='tanh'),
                keras.layers.Dense(self.p, activation=None)
                ])
            )
        else:
            self.l = l

        # SPD:
        if not tau:
            self.tau = cholesky(self.l, self.p, self.vs, self.ws)
        else:
            self.tau = tau

        # Branch net:
        if not beta:
            self.beta = keras.models.Sequential([
                keras.layers.Input(shape=(self.m,)),
                keras.layers.Dense(self.bw, activation='tanh'),
                keras.layers.Dense(self.bw, activation='tanh'),
                # Need non-negative output on the last layer for the sspd property.
                keras.layers.Dense(self.p, activation='tanh'),
            ])
        else:
            self.beta = beta

    def get_config(self):
        config = super().get_config()
        config.update({"vs": self.vs,
                       "ws": self.ws,
                       "dim": self.d,
                       "trunk_width": self.tw,
                       "trunks": self.p,
                       "branch_width": self.bw,
                       "f_nodes_number": self.m,
                       "l": self.l,
                       "tau": self.tau,
                       "beta": self.beta})
        return config

    @classmethod
    def from_config(cls, config):
        config["vs"] = keras.saving.deserialize_keras_object(config["vs"])
        config["ws"] = keras.saving.deserialize_keras_object(config["ws"])
        config["l"] = keras.saving.deserialize_keras_object(config["l"])
        config["tau"] = keras.saving.deserialize_keras_object(config["tau"])
        config["beta"] = keras.saving.deserialize_keras_object(config["beta"])
        return cls(**config)

    def __test_build(self):
        """Reimplements the previous sample factor by fiddling with the weights."""
        weights = [np.array([[1., 2., 3.],[2., 4., 6.]]), np.array([0., 0., 0.])]
        self.l.set_weights(weights)
        print(self.l.get_weights())

    def build(self, input_shape):
        self.l.build(input_shape)
        self.tau.build(input_shape)
        self.beta.build(input_shape)

    def call(self, inputs):
        fs, x, phis = inputs[0], inputs[1], inputs[2]

        # Calculate the arguments for the left factor tau_i(j,k).
        xvs = tf.stack((tf.repeat(x, self.ws.shape[-1]), tf.tile(self.vs, [tf.shape(x)[-1]])), axis = 1)
        txvs = self.tau(xvs)

        # Gather terms with equal j in one dimention.
        txvs = tf.reshape(txvs, shape=[tf.shape(x)[0], len(self.ws), self.p])

        # Calculate the Gauss-Legendre quadrature sum_k tau_i(j, k) phi(k) w(k).
        # '...ki,...k,k->...i'
        quadrature = tf.einsum('jki,jk,k->ji', txvs, phis, self.ws)
        return tf.einsum('ji,ji->j', self.beta(fs) + 1.0, quadrature)


@keras.saving.register_keras_serializable()
class lower_triangular(keras.layers.Layer):
    """Returns a lower triangular version (x = input[..., 0] > y = input[..., 1] => l(x, y) = 0) of the layer provided."""
    def __init__(self, l, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.l = l

    def get_config(self):
        config = super().get_config()

        config.update({"l": self.l})
        return config

    @classmethod
    def from_config(cls, config):
        config["l"] = keras.saving.deserialize_keras_object(config["l"])
        return cls(**config)

    #def build(self, input_shape):
    #    self.l.build(input_shape)

    def call(self, inputs):
        return tf.math.multiply(self.l(inputs),
            tf.expand_dims(1. - keras.ops.sign(keras.ops.relu(inputs[...,1] - inputs[...,0])), -1))

@keras.saving.register_keras_serializable()
class AppendConst(keras.layers.Layer):
    """Append constant const_val to the input."""
    def __init__(self, const_val, *args, **kwargs):
        super().__init__()
        self.const = const_val

    def build(self, input_shape):
        new_shape = input_shape[:-1] + self.const.shape
        multiples = tf.reduce_prod(input_shape[:-1])
        self.const = tf.reshape(tf.tile(self.const, [multiples]), new_shape)
    def call(self, inputs, **kwargs):
        xvs = tf.repeat(inputs, self.const.shape[-1], axis=1)
        #xvs = self.const
        xvs = tf.stack([xvs, self.const])
        xvs = tf.reshape(xvs, shape=[tf.shape(inputs)[0], 2])
        return xvs

@keras.saving.register_keras_serializable()
class kernelIntegration(keras.layers.Layer):
    """Layer implementing the application of a kernel to a function."""
    def __init__(self, kernel, function, vs, ws, **kwargs):
        """Construct a kernel application layer given the kernel, function, quadrature nodes vs and weights ws."""
        super().__init__(**kwargs)
        self.l = l
        self.vs = vs
        self.ws = ws
        self.kernel = kernel
        self.function = function

    def build(self, input_shape):
        pass

    def call(self, inputs):
        pass

@keras.saving.register_keras_serializable()
class cholesky(keras.layers.Layer):
    """Layer implementing an spd function f = int u u^t.

    The integration is approximated via a Gauss-quadrature.

    The Cholesky factor u is given as a tf.model."""

    def __init__(self, l, p, vs, ws, **kwargs):
        """Construct a Cholesky layer given the factor l, quadrature nodes vs and weights ws."""
        super().__init__(**kwargs)
        self.l = l
        self.p = p
        self.vs = vs
        self.ws = ws

    #def build(self, input_shape):
    #    self.l.build(input_shape)

    def get_config(self):
        config = super().get_config()

        config.update({"l": self.l,
                "vs": self.vs,
                "ws": self.ws})
        return config

    @classmethod
    def from_config(cls, config):
        config["vs"] = keras.saving.deserialize_keras_object(config["vs"])
        config["ws"] = keras.saving.deserialize_keras_object(config["ws"])
        config["l"] = keras.saving.deserialize_keras_object(config["l"])
        return cls(**config)

    def call(self, inputs):
        # Separate the inputs into left and right arguments of l.
        xs = inputs[...,0]
        ys = inputs[...,1]

        # Calculate the arguments for the left factor l_i(j,k).
        xvs = tf.stack((tf.repeat(xs, self.ws.shape[-1]), tf.tile(self.vs, tf.shape(xs))), axis = 1)
        lxvs = self.l(xvs)
        # Gather terms with equal j in one dimention.
        lxvs = tf.reshape(lxvs, shape=[tf.shape(xs)[0], len(self.ws), self.p])


        # Calculate the arguments for the right factor l_i(l, k).
        yvs = tf.stack((tf.repeat(ys, len(self.ws)), tf.tile(self.vs, tf.shape(ys))), axis = 1)
        lyvs = self.l(yvs)
        # Gather terms with equal l in one dimension.
        lyvs = tf.reshape(lyvs, shape=[tf.shape(ys)[0], len(self.ws), self.p])

        # Calculate the Gauss-Legende quadrature sum_k l_i(j, k) l_i(l, k) w(k).
        return tf.einsum('...ki,...ki,k->...i' , lxvs, lyvs, self.ws)


keras.layers.Layer()

if __name__ == '__main__':
    print(f"we run")

    #ep = .25
    #l.set_weights([np.array([[-.5, -.5, .5, .5], [-.5, .5, -.5, .5]]), np.array([-ep, -ep, -ep, -ep]),
    #    1./(1. - ep) * np.array([[np.sqrt(2)], [0.], [np.sqrt(.5)], [np.sqrt(3/2.)]]), np.array([0.])])


    vs = tf.constant([-1., 1.])
    ws = tf.constant([1., 1.])

    l = keras.models.Sequential([
        keras.layers.Input(shape=(2,)),
        keras.layers.Dense(4, activation='relu'),
        keras.layers.Dense(1, activation=None),
    ])

    factor = keras.models.Sequential([
        keras.layers.Input(shape=(2,)),
        keras.layers.Dense(40, activation='tanh'),
        keras.layers.Dense(40, activation='tanh'),
        keras.layers.Dense(40, activation='tanh'),
        keras.layers.Dense(1, activation=None),
    ])

    lone = l1(factor)
    model = keras.models.Sequential([cholesky(lone, 1, vs, ws)])
    # model = keras.models.Sequential([l1(factor)])
    model.compile(optimizer=optimizer,
                  loss=keras.losses.MeanSquaredError())

    x_test = tf.constant([[-1., -1.], [-1., 1.], [1., -1.], [1., 1.]])
    y_test = tf.constant([2., 1., 1., 2.])
    # y_test = tf.constant([np.sqrt(2), 0., np.sqrt(.5), np.sqrt(3/2.)])

    x_train = tf.repeat(x_test, repeats=[10, 10, 10, 10], axis=0)
    y_train = tf.repeat(y_test, repeats=[10, 10, 10, 10])

    model.fit(x_train, y_train, epochs=epochs, callbacks=[tensorboard_callback])

    # x_train = tf.random.uniform([100000, 2], minval= -2., maxval = 2)
    # print(f"Shape of xtrain: {tf.shape(x_train)}")
    # y_train = model(x_train)

    # l.set_weights([np.random.normal(size=[2, 4]), np.random.normal(size=[4]),
    #               np.random.normal(size=[4, 1]), np.random.normal(size=[1])])

    # ltwo = l1(factor)
    # model2 = keras.models.Sequential([cholesky(ltwo, 1, vs, ws)])
    # model2.compile(optimizer=optimizer,
    #              loss=keras.losses.MeanSquaredError())

    print(f"Values for {x_test} are (spd) {model(x_test)}")

    # q_nodes = np.array([0., 1], dtype='float32')
    # q_weights = np.array([1., 1.], dtype='float32')

    # n = kernelnet((q_nodes, q_weights), 1)
    # weights = n.get_weights()
    # weights[0] = np.array([[[np.sqrt(2.), 0.], [np.sqrt(.5), np.sqrt(1.5)]]])
    # n.set_weights(weights)
    # print(f"weights: {n.get_weights()}")

    # print(n([tf.constant([[1., 0.], [0., 1.]]),
    #         tf.constant([[1., 0.], [0., 1.]]),
    #         tf.constant([1., 1.])]))

    #print(f"{keras.losses.MeanSquaredError()(model(x_train), y_train)}")

    #x_train_beta = x_train[0]
    #y_train_beta = model.beta(x_train_beta)
    #epochs_beta = 100

    #model_for_beta = kernelnet_continuous(vs=v_nodes, ws=ws)
    #print(f"initial loss: {keras.losses.MeanSquaredError().call(model_for_beta.beta(x_train_beta), y_train_beta)}")

    #lr_beta = 0.001
    #optimizer_beta = keras.optimizers.Adam(learning_rate=lr_beta)

    #model_for_beta.beta.compile(optimizer=optimizer_beta,
    #        loss=keras.losses.MeanSquaredError())

    #model_for_beta.beta.fit(x_train_beta, y_train_beta,
    #    validation_split=0.2,
    #    epochs = epochs_beta)

    #print(f"{y_train_beta - model.beta(x_train_beta)}")

    # y_train_l = model.l(x_train)
    # y_train_tau = model.tau(x_train)



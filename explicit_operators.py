#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
def linear_slab_1d(f, v):
    """Implements Q(f,v) = 1/2 int_{-1}^1 f(v') dv' - f(v)."""
    sigma = 1./2
    dv = v[1] - v[0]
    I =  ( np.sum(f, axis=1) - f[:,0]/2 -  f[:,-1]/2 ) * dv
    II = np.matlib.repmat(I.reshape(len(I), 1), 1, len(v))
    Q = sigma * II - f
    # Q =  sigma - f
    return Q

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt

data = np.load('kernelnet-1720704010.0379682.npz')

x_pred = data['x_pred']
q_pred = data['q_pred']
q_true = data['q_true']

print(q_pred)

plt.plot(x_pred[1,:], q_pred[1,:], )
plt.plot(x_pred[1,:], q_true[1,:])
plt.show()
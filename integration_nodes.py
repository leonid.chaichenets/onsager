import tensorflow as tf
import numpy as np

if __name__ == '__main__':
	m = 24
	xs = tf.range(m)
	xs = tf.reshape(xs, (3,8))
	#print(f"input values: {xs}")

	n = 7
	vs = tf.range(n) + 1
	#print(f"integration nodes: {vs}")

	new_shape = tf.concat([tf.ones(tf.rank(xs) - tf.rank(vs), dtype=tf.dtypes.int32), tf.shape(vs)], axis=0)
	#print(f"new shape of vs {new_shape}")
	s = tf.reshape(vs, new_shape)
	#print(f"corrected last axis: {s}")

	s = tf.expand_dims(s, axis=-2)
	#print(f"added one more axis to vs: {s}")
	#s = tf.repeat(s, [xs.shape[-1]], axis=-2)
	s = tf.tile(s,tf.concat([xs.shape, [1]], axis=-1))
	#print(f"tiling pattern: {xs.shape[:-1]}")
	#print(f"new vs: {s}")

	r = tf.expand_dims(xs, axis=-1)
	r = tf.repeat(r, [vs.shape[-1]], axis = -1)
	#print(f"output: {r}")

	t = tf.stack([r,s], axis=-1)

	print(f"t: {t}")

	#print(tf.linalg.diag_part(tf.constant([[1., 2., 5.],[3., 4, 6.]])))